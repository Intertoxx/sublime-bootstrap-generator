# Bootstrap Generator #

### Bootstrap Generateur C'est Quoi ? ###

Bootstrap Generator est un plugin pour SUblime Text qui permet de crée simplement un projet avec bootstrap en téléchargeant les fichiers pour vous et vous donnera le code minimal pour son fonctionnement

### Comment l'installer ? ###

Dans le menu du haut ouvrez les parametre puis Parcourir les Packages (Browse Packages)
Clonez a l'interieur afin d'avoir un dossier contenant les fichiers python et les fichiers de configuration ou extrayez l'archive que vous avez au préalable téléchargé

### Comment l'utiliser ? ###

Crée un fichier dans le dossier de votre choix puis ouvrez la fenetre promp avec Ctrl+Alt+P, entrez Generate Bootstrap, patientez, codez


### Liens utiles ###
[Fichier zip](https://bitbucket.org/Intertoxx/sublime-bootstrap-generator/get/master.zip)