import sublime, sublime_plugin
import re
import os
import urllib
import urllib2

class GenerateBootstrapCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        if self.internet_on() == True:
            folder = ["css","js","fonts","img"]
            fichiers = [["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css","css/bootstrap.min.css"],["http://miloud.xyz/.hide/base_style.css","css/style.css"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js","js/bootstrap.min.js"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/fonts/glyphicons-halflings-regular.ttf","fonts/glyphicons-halflings-regular.ttf"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/fonts/glyphicons-halflings-regular.woff2","fonts/glyphicons-halflings-regular.woff2"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/fonts/glyphicons-halflings-regular.woff","fonts/glyphicons-halflings-regular.woff"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/fonts/glyphicons-halflings-regular.svg","fonts/glyphicons-halflings-regular.svg"],["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/fonts/glyphicons-halflings-regular.eot","fonts/glyphicons-halflings-regular.eot"]]
            telechargeur = urllib.URLopener()
            current_file = self.view.file_name()
            current_folder = os.path.dirname(current_file) + "/"
            html = "<!DOCTYPE html>\n<html lang=\"fr\">\n\t<head>\n\t\t<meta charset=\"utf-8\">\n\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\t\t<title>Bootstrap</title>\n\t\t<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n\t\t<link href=\"css/style.css\" rel=\"stylesheet\">\n\t\t<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">\n\t\t<!--[if lt IE 9]>\n\t\t<script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>\n\t\t<script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>\n\t\t<![endif]-->\n\t</head>\n\t<body>\n\t\t<h1>Hello, world!</h1>\n\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>\n\t<script src=\"js/bootstrap.min.js\"></script>\n\t</body>\n</html>"

            for dossier in folder:
                if not os.path.exists(current_folder + dossier):
                    os.makedirs(current_folder + dossier)
            for i,fichier in enumerate(fichiers):
                if not os.path.exists(current_folder + fichier[1]):
                    print "Telechargement de " + fichier[1]
                    telechargeur.retrieve(fichier[0],current_folder + fichier[1])
            if self.view.size() > 0:
                if sublime.ok_cancel_dialog("Ce fichier contiens deja du texte, voulez vous vraiment inserer le code de bootstrap ?"):
                    self.view.insert(edit,0,html)
                    sublime.message_dialog("Installation de bootstrap terminee")
                else:
                    return
                pass
            else:
                self.view.insert(edit,0,html)
                sublime.message_dialog("Installation de bootstrap terminee")
            pass
        else:
                sublime.error_message("Vous devez avoir acces a internet pour utiliser ce plugin")


    def internet_on(self):
        try:
            response=urllib2.urlopen('http://google.com',timeout=20)
            return True
        except urllib2.URLError as err: pass
        return False